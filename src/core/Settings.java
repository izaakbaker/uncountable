package core;

@FunctionalInterface
public interface Settings {
    void set();
}
