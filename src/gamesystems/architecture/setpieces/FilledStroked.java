package gamesystems.architecture.setpieces;

import joml.Vector3f;

public interface FilledStroked {
    void setFillColor(Vector3f color);
    void setStrokeColor(Vector3f color);
}
