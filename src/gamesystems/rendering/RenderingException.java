package gamesystems.rendering;

public class RenderingException extends Exception {
    public RenderingException(String message) {
        super(message);
    }
}
